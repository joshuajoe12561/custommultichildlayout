import 'package:custom_layout/customLayoutDelegate.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);
setOffset(Offset offset){
  return offset;
}
  @override
  Widget build(BuildContext context) {
    Offset offset = Offset.zero;
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: CustomMultiChildExampleScreen(offset: setOffset,),
    );
  }
}
class CustomMultiChildExampleScreen extends StatefulWidget {

  const CustomMultiChildExampleScreen({Key? key,  required this.offset}) : super(key: key);
  final Function offset;
  @override
  _CustomMultiChildExampleScreenState createState() => _CustomMultiChildExampleScreenState();
}

class _CustomMultiChildExampleScreenState extends State<CustomMultiChildExampleScreen> {

  List<Widget> dropped = [];
  GlobalKey key = new GlobalKey();
  Offset _offset=Offset.zero;
  @override
  Widget build(BuildContext context) {
  Size size = MediaQuery.of(context).size;

    return Scaffold(
      appBar: AppBar(
        title: Text('CustomMultiChild '),
      ),
      body: Row(
        children: [
          Padding(
              padding: const EdgeInsets.all(8.0),
              child: Container(
                height: size.height,
                width: size.width*1/3,
                color: Colors.lightBlueAccent,
                child: ListView.builder(
                  itemCount: list.length,
                  itemBuilder: (_,index)=>Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Draggable(
                      data: index,
                      key: Key(index.toString()),
                      feedback: list[index],
                      child: list[index],
                      onDragEnd: (details){
                        if(details.wasAccepted){
                          RenderBox render = key.currentContext!.findRenderObject() as RenderBox;
                          print(index);
                            _offset = widget.offset( render.globalToLocal(details.offset));
                        }
                      },
                    ),
                  ),
                ),
              )
          ),
          Expanded(
            child: Container(
              key: key,
              child: DragTarget<int>(
                onWillAccept: (data) {
                          return true;
                      },
                builder: (context, candidateData, rejectedData) {
                        return CustomMultiChildLayout(
                            delegate: YourLayoutDelegate(
                                position: _offset),
                            children:dropped
                        );
                    },
                onAccept: (data) {
                  dropped.add(
                      LayoutId(
                          id:dropped.length,
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: list[data],
                          )));
                  },
            ),
            ),
          )
        ],
      ),
    );
  }
  List<Widget> list = [
    Container(
  decoration: BoxDecoration(
  shape: BoxShape.rectangle,
  color: Colors.purpleAccent
  ),
  child: Center(child: Text('1')),
  height: 75.0,
  width: 75.0,
  ),
  Container(
  decoration: BoxDecoration(
  shape: BoxShape.rectangle,
  color: Colors.purpleAccent
  ),
  child: Center(child: Text('2')),
  height: 75.0,
  width: 75.0,
  ),
  Container(
  decoration: BoxDecoration(
  shape: BoxShape.rectangle,
  color: Colors.purpleAccent
  ),
  child: Center(child: Text('3')),
  height: 75.0,
  width: 75.0,
  ),
  Container(
  decoration: BoxDecoration(
  shape: BoxShape.rectangle,
  color: Colors.purpleAccent
  ),
  child: Center(child: Text('4')),
  height: 75.0,
  width: 75.0,
  ),
  Container(
  decoration: BoxDecoration(
  shape: BoxShape.rectangle,
  color: Colors.purpleAccent
  ),
  child: Center(child: Text('5')),
  height: 75.0,
  width: 75.0,
  ),];
}