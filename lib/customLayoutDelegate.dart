import 'package:flutter/material.dart';


class YourLayoutDelegate extends MultiChildLayoutDelegate {
  // You can pass any parameters to this class because you will instantiate your delegate
  // in the build function where you place your CustomMultiChildLayout.
  // I will use an Offset for this simple example.

  YourLayoutDelegate({required this.position});

  final Offset position;

  @override
  void performLayout(Size size) {
    // `size` is the size of the `CustomMultiChildLayout` itself.

    Size leadingSize = Size
        .zero; // If there is no widget with id `1`, the size will remain at zero.
    // Remember that `1` here can be any **id** - you specify them using LayoutId.
    if (hasChild(0)) {
      leadingSize = layoutChild(
        0, // The id once again.
        BoxConstraints.tightFor(
            width: size.width/2), // This just says that the child cannot be bigger than the whole layout.
      );
      positionChild(
        0,position,);
      // No need to position this child if we want to have it at Offset(0, 0).
    }
  }

  @override
  bool shouldRelayout(YourLayoutDelegate oldDelegate) {
    return oldDelegate.position != position;
  }
}